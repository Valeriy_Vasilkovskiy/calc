package presenter;

import Interface.IHistoryFrame;
import Interface.IPresenterHistory;
import model.History;

import java.util.ArrayList;

public class PresenterForHistory implements IPresenterHistory {
    private IHistoryFrame viewHistory;
    private History historyModel;

    public PresenterForHistory(IHistoryFrame viewHistory) {
        this.viewHistory = viewHistory;
        this.historyModel = new History();
    }

    @Override
    public String history(ArrayList<String> arr, boolean turn) {
        return viewHistory.messageFromHistory(historyModel.HistoryClearOrOpen(arr, turn));
    }
}
