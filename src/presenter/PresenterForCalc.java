package presenter;

import Interface.IMainFrame;
import Interface.IPresenterCalc;
import model.Calc;

import java.io.*;

public class PresenterForCalc implements IPresenterCalc {
    private Calc calc;
    private IMainFrame view;

    public PresenterForCalc(IMainFrame view) {
        this.calc = new Calc();
        this.view = view;
    }

    @Override
    public void calculation(String string) throws IOException {
        view.message(calc.resultOfCalculatoin(string));
    }
}
