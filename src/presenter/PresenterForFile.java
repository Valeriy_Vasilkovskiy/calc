package presenter;

import Interface.IFileFrame;
import Interface.IPresenterFile;
import model.File;

import java.io.IOException;
import java.util.ArrayList;

public class PresenterForFile implements IPresenterFile {
    private File filemodel;
    private IFileFrame viewFile;

    public PresenterForFile(IFileFrame viewFile) {
        this.filemodel = new File();
        this.viewFile = viewFile;
    }

    @Override
    public String file(ArrayList<String> arr) throws IOException {
        return viewFile.messageForFile(filemodel.writeFile(arr));
    }

    @Override
    public String deleteFile() {
        return viewFile.messageForFile(filemodel.deleteFile());
    }
}
