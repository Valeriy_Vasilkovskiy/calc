package Interface;

import java.util.ArrayList;

public interface IPresenterHistory {
    String history(ArrayList<String> arr, boolean turn);
}
