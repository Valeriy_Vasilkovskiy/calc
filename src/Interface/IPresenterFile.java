package Interface;

import java.io.IOException;
import java.util.ArrayList;

public interface IPresenterFile {
    String file(ArrayList<String> arr) throws IOException;

    String deleteFile();
}
