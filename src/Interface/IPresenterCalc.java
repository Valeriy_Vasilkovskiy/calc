package Interface;

import java.io.IOException;

public interface IPresenterCalc {
    void calculation(String string) throws IOException;
}
