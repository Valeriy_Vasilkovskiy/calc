package Interface;

import java.io.IOException;

public interface IMainFrame {
    void message(String string) throws IOException;
}
