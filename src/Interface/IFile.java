package Interface;

import java.io.IOException;
import java.util.ArrayList;

public interface IFile {
    String writeFile(ArrayList<String> arr) throws IOException;

    String deleteFile();
}
