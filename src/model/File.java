package model;

import Interface.IFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class File implements IFile {

//    private static File instance;

    private java.io.File file;

    public File() {
        String desktop = System.getProperty("user.home") + "/Desktop";
        file = new java.io.File(desktop, "Calculate History.txt");
    }

    @Override
    ///////////////////////Возможно добавить изначальную запись из другого файла
    public String writeFile(ArrayList<String> arr) throws IOException {
        String result = null;
        FileWriter writer = new FileWriter(file, true);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);
        if (arr.size() != 0) {
            int numer = 1;
            Date date = new Date();
            bufferedWriter.write("<Сохранение было произведено: " + new SimpleDateFormat("dd MMMM yyyy ',' HH:mm'>'").format(date));

            bufferedWriter.newLine();
            bufferedWriter.write(numer + ")");
            for (int i = 0; i != arr.size(); i++) {
                if (arr.get(i).equals(";")) {
                    numer++;
                    bufferedWriter.write(";");
                    bufferedWriter.newLine();
                    if (i != arr.size() - 1) {
                        bufferedWriter.write(numer + ")");
                    }
                } else {
                    bufferedWriter.write(arr.get(i));
                }
            }
            bufferedWriter.flush();
            bufferedWriter.close();
            result = "Файл успешно сохранен!";
        } else {
            result = "Записывать нечего! XD ";
        }
        return result;
    }

    @Override
    public String deleteFile() {
        String ans = null;
        if (file.delete()) {
            ans = "Файл удалён";
        } else {
            ans = "Файла ещё не существует";
        }
        return ans;
    }
}
