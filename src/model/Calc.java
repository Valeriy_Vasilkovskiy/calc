package model;

import Interface.ICalc;

import java.util.ArrayList;


public class Calc implements ICalc {

    @Override
    public String resultOfCalculatoin(String answer) {
        return sWitch(workWithString(answer));
    }

    private double plus(String first, String second) {
        double a = Double.parseDouble(first);
        double b = Double.parseDouble(second);
        return a + b;
    }


    private double minus(String first, String second) {
        double a = Double.parseDouble(first);
        double b = Double.parseDouble(second);
        return a - b;
    }

    private double division(String first, String second) {
        double a = Double.parseDouble(first);
        double b = Double.parseDouble(second);
        return a / b;
    }

    private double sqrt(String first) {
        double a = Double.parseDouble(first);
        return Math.sqrt(a);
    }

    private double mult(String first, String second) {
        double a = Double.parseDouble(first);
        double b = Double.parseDouble(second);
        return a * b;
    }

    private String IntOrDouble(double num) {
        boolean isWasInt = false;
        String result = String.valueOf(num);
        char[] arr = result.toCharArray();
        if (arr[arr.length - 2] == '.' && arr[arr.length - 1] == '0') {
            isWasInt = true;
        }
        String answer;
        if (isWasInt) {
            int a = (int) num;
            answer = String.valueOf(a);
        } else {
            answer = String.valueOf(num);
        }
        return answer;
    }

    private String[] workWithString(String string) {

        char[] stringToArr = string.toCharArray();
        ArrayList<String> bufMass = new ArrayList<>(); //Складируемый массив
        for (char c : stringToArr) {
            if ((int) c == 46) {
                bufMass.add(String.valueOf(c));
            } else {
                if ((int) c > 47 && (int) c < 58) {
                    //заполнение цифрой;
                    bufMass.add(String.valueOf(c));
                } else {
                    bufMass.add(";");
                    bufMass.add(String.valueOf(c));
                    bufMass.add(";");
                }
            }
        }
        String array = "";
        for (String e : bufMass) {
            array = array.concat(e);                           //Объединение в одну строку и удаление разделяющего знака (;)
        }
        String[] mainNumArray = array.split(";");
        ////////////////////////////////////////////////////
        String[] finArr = new String[mainNumArray.length];
        System.arraycopy(mainNumArray, 0, finArr, 0, mainNumArray.length);
        return finArr;
    }
    ///////////////////////////////////////////////////////

    private String sWitch(String[] arr) {
        double result = 0;
        int k = 0;
        int z = 0;
        if (arr.length == 1) {
            return arr[0];
        }
        ////////////////////////////////////////////
        ArrayList<String> finalArray = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            finalArray.add(i, arr[i]);
        }
        //////////////////////////////
        ArrayList<Integer> leftS = new ArrayList<>();
        ArrayList<Integer> rigntS = new ArrayList<>();
        int size = finalArray.size();
        for (int i = 0; i != size; i++) {
            for (int j = i; j != size; j++) {   ///////////////////////////////////? i=1
                ///////////////////////////////ввести скобки
                if (j == finalArray.size() || j > finalArray.size()) {
                    break;
                }
                if (finalArray.get(i).equals("(")) {
                    leftS.add(i);
                    break;
                }
                if (finalArray.get(i).equals(")")) {
                    rigntS.add(i);
                    break;
                }
                if (!finalArray.get(i).equals("(") || !finalArray.get(i).equals(")")) {
                    break;
                }
            }
        }
        if (rigntS.size() != leftS.size()) {
            return "Лишние скобки";
        } else if (rigntS.size() != 0) {
            for (int i = 0; i < finalArray.size(); i++) {
                for (int j = i; j < finalArray.size(); j++) {
                    if (finalArray.get(i).equals("")) {
                        finalArray.remove(i);
                        break;
                    }
                }
            }
            //////////////////////////////////////////Вот тут скорее всего очень тяжко, пересмотреть и упростить
            do {
                refreshHoods(finalArray, leftS, rigntS);
                deleteAllHoods(finalArray, leftS, rigntS);
            } while (leftS.size() != 0 && rigntS.size() != 0);
            ////////////////////////////////////////////////////////////////////////////сделать рекурсию
        }
        operationWithSignMultAndDivide(finalArray);
        ///////////////////////////////////////////Чекнуть;
        if (finalArray.get(0).equals("")) {
            if (finalArray.get(1).equals("-")) {
                double i = Double.parseDouble(finalArray.get(2));
                i *= 2;
                finalArray.add(finalArray.size(), "-");
                finalArray.add(finalArray.size(), String.valueOf(i));
                finalArray.remove(0);
                finalArray.remove(0);
            }
            if (finalArray.get(1).equals("√")) {
                finalArray.remove(0);
            }
        }
        /////////////////////////////////////////////////
        if (finalArray.size() == 1) {
            result = Double.parseDouble(finalArray.get(0));
        } else {
            for (int i = 0; i < finalArray.size(); i++) {
                for (int j = i; j != finalArray.size(); j++) {
                    if (k == 0) {
                        switch (finalArray.get(i)) {
                            case "+":
                                if (z == 0) {
                                    result += plus(finalArray.get(j - 1), finalArray.get(j + 1));
                                    z++;
                                    k++;
                                } else {
                                    result = plus(String.valueOf(result), finalArray.get(j + 1));
                                    k++;
                                }
                                break;
                            case "-":
                                if (z == 0) {
                                    result += minus(finalArray.get(j - 1), finalArray.get(j + 1));
                                    z++;
                                    k++;
                                } else {
                                    result = minus(String.valueOf(result), finalArray.get(j + 1));
                                    k++;
                                }
                                break;
                            case "*":
                                if (z == 0) {
                                    result += mult(finalArray.get(j - 1), finalArray.get(j + 1));
                                    z++;
                                    k++;
                                } else {
                                    result = mult(String.valueOf(result), finalArray.get(j + 1));
                                    k++;
                                }
                                break;
                            case "/":
                                if (z == 0) {
                                    result += division(finalArray.get(j - 1), finalArray.get(j + 1));
                                    z++;
                                    k++;
                                } else {
                                    result = division(String.valueOf(result), finalArray.get(j + 1));
                                    k++;
                                }
                                break;
                            case "√":
                                result += sqrt(finalArray.get(j + 1));
                                k++;
                                z++;
                                break;
                        }
                    } else {
                        k = 0;
                        break;
                    }
                }
            }
            if (result == 0 && finalArray.size() == 1) {
                result = Double.parseDouble(finalArray.get(0));
            }
        }

        return IntOrDouble(result);
    }

    private void deleteAllHoods(ArrayList<String> finalArray, ArrayList<Integer> leftS, ArrayList<Integer> rigntS) {
        int startForHoods = 0;
        int endForHoods = 0;
        int left = 0;
        int right = 0;
        String ans = null;
        if (leftS.get(leftS.size() - 1) > rigntS.get(0)) {
            for (int i = leftS.size() - 1; i >= 0; i--) {
                if (leftS.get(i) < rigntS.get(0)) {
                    left = leftS.get(i);
                    right = rigntS.get(0);
                    break;
                }
            }
        } else {
            left = leftS.get(leftS.size() - 1);
            right = rigntS.get(0);
        }

        if (finalArray.get(left).equals("(")) {
            if (left != 0) {
                if (!finalArray.get(left - 1).equals("+") && !finalArray.get(left - 1).equals("-") &&
                        !finalArray.get(left - 1).equals("*") && !finalArray.get(left - 1).equals("/") &&
                        !finalArray.get(left - 1).equals("√") && !finalArray.get(left - 1).equals("(")) {

//                    if(leftS.get(leftS.size()-1)!=leftS.get(leftS.size()-2)+1) {
                    finalArray.remove(right);
                    finalArray.add(left + 1, "*");
                    finalArray.remove(left);
                    startForHoods = left + 1;
                    endForHoods = right - 1;
//                    }
                } else {
                    finalArray.remove(right);
                    finalArray.remove(left);
                    startForHoods = left;
                    endForHoods = right - 2;
                }
            } else {
                finalArray.remove(right);
                finalArray.remove(left);
                endForHoods = right - 2;
            }
            if (endForHoods > startForHoods) {
                for (int i = startForHoods; i < endForHoods + 1; i++) {
                    for (int j = i; j < endForHoods + 1; j++) {
                        if (finalArray.get(j).equals("*") || finalArray.get(j).equals("/")) {
                            String infoAboutSign = finalArray.get(j);
                            if (finalArray.get(j + 1).equals("")) {
                                finalArray.remove(j + 1);
                            }
                            if (finalArray.get(j + 1).equals("√")) {
                                ans = String.valueOf(sqrt(finalArray.get(j + 2)));
                                finalArray.remove(j + 1);
                                finalArray.remove(j + 1);
                                finalArray.add(j + 1, ans);
                                endForHoods -= 1;
                            } else {
                                if (infoAboutSign.equals("*")) {
                                    ans = String.valueOf(mult(finalArray.get(j - 1), finalArray.get(j + 1)));
                                } else if (infoAboutSign.equals("/")) {
                                    ans = String.valueOf(division(finalArray.get(j - 1), finalArray.get(j + 1)));
                                }
                                if (j == finalArray.size()) {
                                    finalArray.add(j, ans);
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 2);
                                    finalArray.remove(j - 3);
                                    endForHoods -= 2;
                                } else {
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 1);
                                    finalArray.add(j - 1, ans);
                                    endForHoods -= 2;
                                }
                                i -= 1;
                                break;
                            }
                            if (j == finalArray.size() || j > finalArray.size()) {
                                break;
                            } else if (finalArray.get(j).equals("√")) {
                                if (finalArray.get(j - 1).equals("")) {
                                    finalArray.remove(j - 1);
                                } else {
                                    ans = String.valueOf(sqrt(finalArray.get(j + 1)));
                                    if (j == finalArray.size()) {
                                        finalArray.add(j, ans);
                                        finalArray.remove(j - 1);
                                        finalArray.remove(j - 2);
                                    } else {
                                        finalArray.add(j, ans);
                                        finalArray.remove(j + 1);
                                        finalArray.remove(j + 1);
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            if (endForHoods > startForHoods) {
                for (int i = startForHoods; i < endForHoods + 1; i++) {
                    for (int j = i; j < endForHoods + 1; j++) {
                        if (finalArray.get(j).equals("+") || finalArray.get(j).equals("-")) {
                            String infoAboutSign = finalArray.get(j);
                            if (finalArray.get(j + 1).equals("")) {
                                finalArray.remove(j + 1);
                                endForHoods -= 1;
                            }
                            if (finalArray.get(j + 1).equals("√")) {
                                ans = String.valueOf(sqrt(finalArray.get(j + 2)));
                                finalArray.remove(j + 1);
                                finalArray.remove(j + 1);
                                finalArray.add(j + 1, ans);
                                endForHoods -= 1;
                            } else {
                                if (infoAboutSign.equals("+")) {
                                    ans = String.valueOf(plus(finalArray.get(j - 1), finalArray.get(j + 1)));
                                } else if (infoAboutSign.equals("-")) {
                                    ans = String.valueOf(minus(finalArray.get(j - 1), finalArray.get(j + 1)));
                                }
                                if (j == finalArray.size()) {
                                    finalArray.add(j, ans);
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 2);
                                    finalArray.remove(j - 3);

                                } else {
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 1);
                                    finalArray.add(j - 1, ans);
                                }
                                endForHoods -= 2;
                                i -= 1;
                                break;
                            }
                        }
                        if (j == finalArray.size() || j > finalArray.size()) {
                            break;
                        } else if (finalArray.get(j).equals("√")) {
                            if (finalArray.get(j - 1).equals("")) {
                                finalArray.remove(j - 1);
                            } else {
                                ans = String.valueOf(sqrt(finalArray.get(j + 1)));
                                if (j == finalArray.size()) {
                                    finalArray.add(j, ans);
                                    finalArray.remove(j - 1);
                                    finalArray.remove(j - 2);
                                    endForHoods -= 1;
                                } else {
                                    finalArray.add(j, ans);
                                    finalArray.remove(j + 1);
                                    finalArray.remove(j + 1);
                                }
                                endForHoods -= 1;
                            }
                            break;
                        }
                    }
                }
            }
            leftS.remove(leftS.size() - 1);
            rigntS.remove(0);
        }
    }

    private void refreshHoods(ArrayList<String> finalArray, ArrayList<Integer> leftS, ArrayList<Integer> rigntS) {
        do {
            leftS.remove(0);
            rigntS.remove(0);
        } while (leftS.size() != 0);
        for (int i = 0; i < finalArray.size(); i++) {
            if (finalArray.get(i).equals("(")) {
                leftS.add(i);
            } else if (finalArray.get(i).equals(")")) {
                rigntS.add(i);
            }
        }
    }

    private void operationWithSignMultAndDivide(ArrayList<String> finalArray) {
        for (int i = 1; i != finalArray.size(); i++) {
            String ans = null;
            for (int j = i; j != finalArray.size(); j++) {
                /////////////////////////////////////////////////////////////////
                if (j > finalArray.size() || i < 0) {
                    break;
                } else if (finalArray.get(j).equals("*") || finalArray.get(j).equals("/")) {
                    String infoAboutSign = finalArray.get(j);
                    if (finalArray.get(j + 1).equals("")) {
                        finalArray.remove(j + 1);
                    }
                    if (finalArray.get(j + 1).equals("√")) {
                        ans = String.valueOf(sqrt(finalArray.get(j + 2)));
                        finalArray.remove(j + 1);
                        finalArray.remove(j + 1);
                        finalArray.add(j + 1, ans);
                    } else {
                        if (infoAboutSign.equals("*")) {
                            ans = String.valueOf(mult(finalArray.get(j - 1), finalArray.get(j + 1)));
                        } else if (infoAboutSign.equals("/")) {
                            ans = String.valueOf(division(finalArray.get(j - 1), finalArray.get(j + 1)));
                        }
                        if (j == finalArray.size()) {
                            finalArray.add(j, ans);
                            finalArray.remove(j - 1);
                            finalArray.remove(j - 2);
                            finalArray.remove(j - 3);
                        } else {
                            finalArray.remove(j - 1);
                            finalArray.remove(j - 1);
                            finalArray.remove(j - 1);
                            finalArray.add(j - 1, ans);
                        }
                        i -= 1;
                        break;
                    }
                }
                if (j == finalArray.size() || j > finalArray.size()) {
                    break;
                } else if (finalArray.get(j).equals("√")) {
                    if (finalArray.get(j - 1).equals("")) {
                        finalArray.remove(j - 1);
                    } else {
                        ans = String.valueOf(sqrt(finalArray.get(j + 1)));
                        if (j == finalArray.size()) {
                            finalArray.add(j, ans);
                            finalArray.remove(j - 1);
                            finalArray.remove(j - 2);
                        } else {
                            finalArray.add(j, ans);
                            finalArray.remove(j + 1);
                            finalArray.remove(j + 1);
                        }
                    }
                    i -= 1;
                    break;
                }
            }
        }
    }
}
