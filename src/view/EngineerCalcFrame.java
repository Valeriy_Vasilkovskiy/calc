package view;

import Interface.IMainFrame;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class EngineerCalcFrame implements IMainFrame {

    private static EngineerCalcFrame instance;
    private JFrame jFrame;

    JFrame getjFrame() {
        return jFrame;
    }

    static EngineerCalcFrame getInstanceEngineer() {
        if (instance == null) {
            instance = new EngineerCalcFrame();
        }
        return instance;
    }

    private EngineerCalcFrame() {
        //////////////////////////////////////////////////Основные параметры Главного окна
        jFrame = new JFrame("Simple Calc");
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(200, 400);
        jFrame.setLocation(MainFrame.getInstanceMainWindow().getjFrame().getLocation());
        jFrame.setResizable(false);
        jFrame.add(jPanel);
        try {
            Image i = ImageIO.read(new File("Иконка калькулятора.jpeg"));
            jFrame.setIconImage(i);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /////////////////////////////////////////////////////////////////////////////////Все кнопки
//        JButton sin = new JButton("Sin");
//        sin.setBounds(20, 120, 60, 60);
//        jPanel.add(sin);
//
//        JButton cos = new JButton("Cos");
//        cos.setBounds(110, 120, 60, 60);
//        jPanel.add(cos);

        JButton leftS = new JButton("(");
        leftS.setBounds(20, 20, 60, 60);
        jPanel.add(leftS);

        JButton rightS = new JButton(")");
        rightS.setBounds(110, 20, 60, 60);
        jPanel.add(rightS);

        jFrame.revalidate();
        jFrame.setVisible(true);
        //////////////////////////////
//        sin.addActionListener(new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                MainFrame.getInstanceMainWindow().writeButtonSigns(e);
//            }
//        });
//        cos.addActionListener(new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                MainFrame.getInstanceMainWindow().writeButtonSigns(e);
//            }
//        });
        leftS.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainFrame.getInstanceMainWindow().writeButtonNumbers(e);
            }
        });
        rightS.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MainFrame.getInstanceMainWindow().writeButtonNumbers(e);
            }
        });
        jFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                MainFrame.getInstanceMainWindow().animationForCalc("close");
                jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        });
    }

    private double startPosX = 0;
    private double endPosX = 0;

    private double startPosY = 0;
    private double endPosY = 0;

    double getStartPosY() {
        return startPosY;
    }

    private void setStartPosY(double startPosY) {
        this.startPosY = startPosY;
    }

    double getEndPosY() {
        return endPosY;
    }

    void setEndPosY(double endPosY) {
        this.endPosY = endPosY;
    }

    double getEndPosX() {
        return endPosX;
    }

    void setEndPosX(double endPosX) {
        this.endPosX = endPosX;
    }

    double getStartPosX() {
        return startPosX;
    }

    private void setStartPosX(double startPosX) {
        this.startPosX = startPosX;
    }

    synchronized void animationOpen(String string) {
        setStartPosX(getjFrame().getX());
        setStartPosY(getjFrame().getY());
        if (string.equals("open")) {
            startPosX -= 2;
        } else if (string.equals("close")) {

            if (MainFrame.getInstanceMainWindow().getjFrame().getX() > startPosX) {
                startPosX += 2;
            } else {
                startPosX -= 2;
            }
            if (MainFrame.getInstanceMainWindow().getjFrame().getY() > startPosY) {
                startPosY += 2;
            } else {
                startPosY -= 2;
            }
        }
        getjFrame().setLocation((int) startPosX, (int) startPosY);
        getjFrame().repaint();
    }

    void deleteHistory() {
        instance = null;
    }


    @Override
    public void message(String string) {

    }
}
