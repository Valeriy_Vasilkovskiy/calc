package view;

import Interface.IHistoryFrame;
import Interface.IPresenterHistory;
import presenter.PresenterForHistory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class HistoryFrame implements IHistoryFrame {
    private IPresenterHistory iPresenterHistory;
    private JFrame jFrameHistory;

    JFrame getjFrameHistory() {
        return jFrameHistory;
    }

    private static HistoryFrame instance;

    static HistoryFrame getInstanceHistory() {
        if (instance == null) {
            instance = new HistoryFrame();
        }
        return instance;
    }

    private JTextArea Texthistory;

    IPresenterHistory getiPresenterHistory() {
        return iPresenterHistory;
    }

    JTextArea getTexthistory() {
        return Texthistory;
    }

    private HistoryFrame() {
        iPresenterHistory = new PresenterForHistory(this);
        jFrameHistory = new JFrame("History");
        JPanel jPanelHistory = new JPanel();
        Texthistory = new JTextArea(10, 20);
        jPanelHistory.setLayout(null);
        jFrameHistory.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        jFrameHistory.setVisible(true);
        jFrameHistory.setSize(300, 400);
        jFrameHistory.setLocation(MainFrame.getInstanceMainWindow().getjFrame().getLocation());
        jFrameHistory.setResizable(false);
        jFrameHistory.add(jPanelHistory);
        Texthistory.setEditable(false);

        Texthistory.setBounds(10, 5, 255, 350);
        Texthistory.setFont(new Font("Arial", Font.PLAIN, 17));
        Texthistory.setEditable(false);
        jPanelHistory.add(Texthistory);
        JScrollBar jScrollBar = new JScrollBar();
        jScrollBar.setBounds(275, 0, 20, 365);
        jPanelHistory.add(jScrollBar);
        jFrameHistory.add(new JScrollPane(Texthistory));
        ////////////////////////////////////////////////////////
        jFrameHistory.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                MainFrame.getInstanceMainWindow().animationForHistory("close");
                jFrameHistory.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        });
    }


    private double startPosX = 0;
    private double endPosX = 0;

    private double startPosY = 0;
    private double endPosY = 0;

    double getStartPosY() {
        return startPosY;
    }

    private void setStartPosY(double startPosY) {
        this.startPosY = startPosY;
    }

    double getEndPosY() {
        return endPosY;
    }

    void setEndPosY(double endPosY) {
        this.endPosY = endPosY;
    }

    double getEndPosX() {
        return endPosX;
    }

    void setEndPosX(double endPosX) {
        this.endPosX = endPosX;
    }

    double getStartPosX() {
        return startPosX;
    }

    private void setStartPosX(double startPosX) {
        this.startPosX = startPosX;
    }

    synchronized void animationOpen(String string) {
        setStartPosX(getjFrameHistory().getX());
        setStartPosY(getjFrameHistory().getY());
        if (string.equals("open")) {
            startPosX += 2;
        } else if (string.equals("close")) {

            if (MainFrame.getInstanceMainWindow().getjFrame().getX() > startPosX) {
                startPosX += 2;
            } else {
                startPosX -= 2;
            }
            if (MainFrame.getInstanceMainWindow().getjFrame().getY() > startPosY) {
                startPosY += 2;
            } else {
                startPosY -= 2;
            }

        }
        getjFrameHistory().setLocation((int) startPosX, (int) startPosY);
        getjFrameHistory().repaint();
    }

    void deleteHistory() {
        instance = null;
    }

    @Override
    public String messageFromHistory(String string) {
        return string;
    }
}
