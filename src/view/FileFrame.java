package view;

import Interface.IFileFrame;
import Interface.IPresenterFile;
import presenter.PresenterForFile;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.*;


public class FileFrame implements IFileFrame {
    private IPresenterFile iPresenterFile;

//    private static FileFrame instance;
//
//    static FileFrame getInstanceFile(String s) throws IOException {
//        if (instance == null) {
//            instance = new FileFrame(s);
//        }
//        return instance;
//    }

    private IPresenterFile getiPresenterFile() {
        return iPresenterFile;
    }

    FileFrame(String s) throws IOException {
        iPresenterFile = new PresenterForFile(this);
        if (s.equals("open")) {
            JFileChooser openFile = new JFileChooser(System.getProperty("user.home") + "/Desktop");
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Text File", "txt");
            openFile.setAcceptAllFileFilterUsed(false);
            openFile.addChoosableFileFilter(filter);
            int read = 0;
            int ret = openFile.showOpenDialog(null);
            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = openFile.getSelectedFile();
                JFrame FileOpenFrame = new JFrame(file.getName());
                FileOpenFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                FileReader in = null;
                try {
                    in = new FileReader(file);
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
                char[] toTextAreaArray = new char[(int) file.length()];
                try {
                    assert in != null;  //Проверяем данные на правильность
                    read = in.read(toTextAreaArray);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                try {
                    in.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                JTextArea area = new JTextArea(10, 20);
                area.setFont(new Font("Arial", Font.PLAIN, 20));
                area.append(String.valueOf(toTextAreaArray, 0, read));
                FileOpenFrame.getContentPane().add(new JScrollPane(area), BorderLayout.CENTER);
                FileOpenFrame.pack(); //Устанавливает изначальный размер для видимости всех обьектов.
                FileOpenFrame.setVisible(true);
//                instance = null;
            }
        }
        if (s.equals("delete")) {
            jDialog.getInstanceDialog(s).getTextDialog().setText(getiPresenterFile().deleteFile());
//            instance = null;
        }
        if (s.equals("save")) {
            //////////////////////////Создаётся второй mainFrame
            jDialog.getInstanceDialog("saved").getTextDialog().setText(getiPresenterFile().file(MainFrame.getInstanceMainWindow().getArrayListToHistoty()));
            MainFrame.getInstanceMainWindow().getArrayListToHistoty().clear();
//            instance = null;
        }
    }

    @Override
    public String messageForFile(String string) {
        return string;
    }
}
