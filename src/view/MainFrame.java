package view;

import Interface.*;
import presenter.PresenterForCalc;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainFrame implements IMainFrame {
    private static MainFrame instance;

    static MainFrame getInstanceMainWindow() {
        if (instance == null) {
            instance = new MainFrame();
        }
        return instance;
    }

    private IPresenterCalc iPresenterCalc;
    private static JTextField jTextField;
    private static JTextField jTextRes;
    private ArrayList<String> arrayListToHistoty = new ArrayList<>();
    private static int onlyOneDOt = 0;
    private int onlyOneZero = 0;
    private static int unchangeableRes = 0;
    private static int onlyOneChange = 0;
    private JFrame jFrame;
    private boolean checkOpenHistory = false;
    private boolean checkOpenEngeneerCalc = false;

    private void setCheckOpenHistory(boolean checkOpenHistory) {
        this.checkOpenHistory = checkOpenHistory;
    }


    JFrame getjFrame() {
        return jFrame;
    }

    ArrayList<String> getArrayListToHistoty() {
        return arrayListToHistoty;
    }

    private MainFrame() {

        iPresenterCalc = new PresenterForCalc(this);
        //////////////////////////////////////////////////Основные параметры Главного окна
        jFrame = new JFrame("Simple Calc");
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300, 400);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);
        try {
            Image i = ImageIO.read(new File("Иконка калькулятора.jpeg"));
            jFrame.setIconImage(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ///////////////////////////////////Меню выбора
        JMenuBar jMenuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu history = new JMenu("History");
        JMenu calculator = new JMenu("Calculator");
        jMenuBar.add(file);
        jMenuBar.add(history);
        jMenuBar.add(calculator);
        jFrame.setJMenuBar(jMenuBar);
        jFrame.revalidate();
        ////////////////////////////////////////////////Настройка кнопок выбора в меню
        //другие калькуляторы
        JMenuItem engineerCalculator = new JMenuItem("Engineer");
        calculator.add(engineerCalculator);

        //File
        JMenuItem saveFile = new JMenuItem("Save");
        JMenuItem openFile = new JMenuItem("Open");
        JMenuItem deleteFile = new JMenuItem("Delete");
        file.add(saveFile);
        file.add(openFile);
        file.add(deleteFile);
        //History
        JMenuItem openHistory = new JMenuItem("Open");
        JMenuItem clearHistory = new JMenuItem("Clear");
        history.add(openHistory);
        history.add(clearHistory);
        //////////////////////////////////////////////////////////////////////////////////////Поле результата
        jTextRes = new JTextField();
        jTextRes.setBounds(20, 5, 260, 15);
        jTextRes.setHorizontalAlignment(4);
        jTextRes.setEditable(false);
        jTextRes.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 2));
        jTextRes.setBackground(Color.WHITE);
        jTextRes.setFont(new Font("Arial", Font.PLAIN, 12));
        jTextRes.setForeground(Color.GREEN);
        jPanel.add(jTextRes);
        ////////////////////////////////////////////////////////////////////////////////////////Поле ввода
        jTextField = new JTextField();
        jTextField.setBounds(20, 18, 260, 30);
        jTextField.setHorizontalAlignment(4);
        jTextField.setEditable(false);
        jTextField.setBackground(Color.WHITE);
        jTextField.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 2));
        jTextField.setFont(new Font("Arial", Font.PLAIN, 15));
        jPanel.add(jTextField);
        /////////////////////////////////////////////////////////////////////////////////Все кнопки
        JButton jButtonBackspace = new JButton("←");
        jButtonBackspace.setBounds(20, 100, 50, 30);
        jPanel.add(jButtonBackspace);

        JButton jButton7 = new JButton("7");
        jButton7.setBounds(20, 150, 50, 30);
        jPanel.add(jButton7);

        JButton jButton8 = new JButton("8");
        jButton8.setBounds(90, 150, 50, 30);
        jPanel.add(jButton8);

        JButton jButton9 = new JButton("9");
        jButton9.setBounds(160, 150, 50, 30);
        jPanel.add(jButton9);

        JButton jButton4 = new JButton("4");
        jButton4.setBounds(20, 200, 50, 30);
        jPanel.add(jButton4);

        JButton jButton5 = new JButton("5");
        jButton5.setBounds(90, 200, 50, 30);
        jPanel.add(jButton5);

        JButton jButton6 = new JButton("6");
        jButton6.setBounds(160, 200, 50, 30);
        jPanel.add(jButton6);

        JButton jButton1 = new JButton("1");
        jButton1.setBounds(20, 250, 50, 30);
        jPanel.add(jButton1);

        JButton jButton2 = new JButton("2");
        jButton2.setBounds(90, 250, 50, 30);
        jPanel.add(jButton2);

        JButton jButton3 = new JButton("3");
        jButton3.setBounds(160, 250, 50, 30);
        jPanel.add(jButton3);

        JButton jButton0 = new JButton("0");
        jButton0.setBounds(20, 300, 120, 30);
        jPanel.add(jButton0);

        JButton jButtonDot = new JButton(".");
        jButtonDot.setBounds(160, 300, 50, 30);
        jPanel.add(jButtonDot);

        JButton jButtonPlus = new JButton("+");
        jButtonPlus.setBounds(230, 150, 50, 80);
        jPanel.add(jButtonPlus);

        JButton jButtonMinus = new JButton("-");
        jButtonMinus.setBounds(230, 100, 50, 30);
        jPanel.add(jButtonMinus);

        JButton jButtonMultiply = new JButton("*");
        jButtonMultiply.setBounds(160, 100, 50, 30);
        jPanel.add(jButtonMultiply);

        JButton jButtonDivide = new JButton("/");
        jButtonDivide.setBounds(90, 100, 50, 30);
        jPanel.add(jButtonDivide);

        JButton jButtonPosOrNeg = new JButton("±");
        jButtonPosOrNeg.setBounds(230, 55, 50, 30);
        jPanel.add(jButtonPosOrNeg);

        JButton jButtonEqual = new JButton("=");
        jButtonEqual.setBounds(230, 250, 50, 80);
        jPanel.add(jButtonEqual);

        JButton jButtonSqrt = new JButton("√");
        jButtonSqrt.setBounds(160, 55, 50, 30);
        jPanel.add(jButtonSqrt);

        JButton jButtonDeleteAll = new JButton("CE");
        jButtonDeleteAll.setFont(new Font("Helvetica", Font.BOLD, 11));
        jButtonDeleteAll.setBounds(90, 55, 50, 30);
        jPanel.add(jButtonDeleteAll);

        JButton jButtonC = new JButton("C");
        jButtonC.setFont(new Font("Helvetica", Font.BOLD, 12));
        jButtonC.setBounds(20, 55, 50, 30);
        jPanel.add(jButtonC);

        jFrame.revalidate();
        jFrame.setVisible(true);

        /////////////////////////////////////////////////////////////////////////////////////////Реализация кнопок

        jButtonC.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jTextField.setText("");
                unchangeableRes = 0;
            }
        });

        jButtonDeleteAll.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jTextField.setText("");
                jTextRes.setText("");
                unchangeableRes = 0;
            }
        });
        jButton0.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (onlyOneZero == 0) {
                    writeButtonNumbers(e);
                    onlyOneZero = 1;
                }
            }
        });
        jButton1.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton2.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton3.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton4.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton5.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton6.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton7.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton8.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButton9.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                writeButtonNumbers(e);
            }
        });
        jButtonBackspace.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (unchangeableRes == 0) {
                    if (jTextField.getText().length() != 0) {
                        String main = jTextField.getText();
                        char[] mainArr = main.toCharArray();
                        char[] buf = new char[mainArr.length - 1];
                        System.arraycopy(mainArr, 0, buf, 0, main.length() - 1);
                        String finBuf = String.valueOf(buf);
                        jTextField.setText(finBuf);
                        ////Проверить
                    }
                }
            }
        });
        jButtonDivide.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                writeButtonSigns(e);
            }
        });
        jButtonDot.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextField.getText().endsWith("+") || jTextField.getText().endsWith("-") ||
                        jTextField.getText().endsWith("/") || jTextField.getText().endsWith("*") || unchangeableRes != 0) {
                } else {
                    if (onlyOneDOt == 0) {
                        writeButtonSigns(e);
                        onlyOneDOt++;
                        onlyOneZero = 0;
                    }
                }
                //Проверить
            }
        });
        jButtonEqual.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextField.getText().length() != 0) {
                    if (!jTextField.getText().equals(jTextRes.getText())) {
                        if(arrayListToHistoty.size()!=0) {
                            if (jTextField.getText().equals(arrayListToHistoty.get(arrayListToHistoty.size() - 2))) {
                                return;
                            }
                        }
                        if (jTextField.getText().endsWith("+") || jTextField.getText().endsWith("-") ||
                                jTextField.getText().endsWith("/") || jTextField.getText().endsWith("*")) {
                            String main = jTextField.getText();
                            char[] mainArr = main.toCharArray();
                            char[] buf = new char[mainArr.length - 1];
                            System.arraycopy(mainArr, 0, buf, 0, main.length() - 1);
                            String finBuf = String.valueOf(buf);
                            jTextField.setText(finBuf);
                        }
                        if (jTextField.getText().startsWith("+")) {
                            String main = jTextField.getText();
                            char[] mainArr = main.toCharArray();
                            char[] buf = new char[mainArr.length - 1];
                            System.arraycopy(mainArr, 1, buf, 0, main.length() - 1);
                            String finBuf = String.valueOf(buf);
                            jTextField.setText(finBuf);
                        }
                        try {
                            iPresenterCalc.calculation(jTextField.getText());
                            unchangeableRes++;
                            onlyOneChange = 0;
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        if (checkOpenHistory) {
                            HistoryFrame.getInstanceHistory().getTexthistory().setText(HistoryFrame.getInstanceHistory()
                                    .getiPresenterHistory().history(arrayListToHistoty, true));

                        }
                    }
                }

            }
        });
        jButtonMinus.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jTextField.getText().length() == 0 || onlyOneSign(jTextField.getText())) {
                    writeButtonNumbers(e);
                }
            }
        });
        jButtonMultiply.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                writeButtonSigns(e);
            }
        });
        jButtonPlus.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                writeButtonSigns(e);

            }
        });
        jButtonPosOrNeg.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (onlyOneChange == 0) {
                    char[] change = jTextField.getText().toCharArray();
                    char[] finArr = new char[change.length + 1];
                    if (change[0] == '-') {
                        change[0] = '+';
                        jTextField.setText(String.valueOf(change));
                    } else if (change[0] != '√') {
                        finArr[0] = '-';
                        System.arraycopy(change, 0, finArr, 1, change.length);
                        jTextField.setText(String.valueOf(finArr));
                    }
                    unchangeableRes = 1;
                    onlyOneChange++;
                }
            }
        });
        jButtonSqrt.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int cheekSign = 0;
                int numberOfLastSign = 0;
                int numInfo = 0;
                if (onlyOneSign(jTextField.getText())) {
                    String string = (jTextField.getText() + e.getActionCommand());
                    char[] array = string.toCharArray();
                    for (int i = array.length - 2; i >= 0; i--) {
                        if ((int) array[i] == 42 || (int) array[i] == 47 || (int) array[i] == 43 || (int) array[i] == 45 ||
                                String.valueOf(array[i]).equals("√")) {
                            numberOfLastSign = i;
                            cheekSign++;
                            break;
                        } else {
                            numInfo++;
                        }
                    }
                    if (array[numberOfLastSign] != '√') {
                        if (cheekSign == 0) {
                            string = e.getActionCommand() + jTextField.getText();
                            jTextField.setText(string);
                        } else {
                            char[] copyWithOutSign = new char[array.length - numInfo - 1];
                            char[] bufNum = new char[numInfo];
                            System.arraycopy(array, 0, copyWithOutSign, 0, numberOfLastSign + 1);
                            System.arraycopy(array, array.length - numInfo - 1, bufNum, 0, numInfo);
                            string = String.valueOf(copyWithOutSign) + e.getActionCommand() + String.valueOf(bufNum);
                            jTextField.setText(string);
                        }
                    }
                }
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////Реализация кнопок меню History
        openHistory.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkOpenHistory) {
                    HistoryFrame.getInstanceHistory().getTexthistory().setText(HistoryFrame.getInstanceHistory()
                            .getiPresenterHistory().history(arrayListToHistoty, true));
                    animationForHistory("open");
                    setCheckOpenHistory(true);
                }
            }
        });
        clearHistory.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HistoryFrame.getInstanceHistory().getTexthistory().setText(HistoryFrame.getInstanceHistory()
                        .getiPresenterHistory().history(arrayListToHistoty, false));
            }
        });

        //////////////////////////////////////////////////////////////////////////////////////Реализация кнопок меню File
        saveFile.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jDialog.getInstanceDialog("save");
            }
        });
        openFile.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ThreadForFile threadForFile = new ThreadForFile("open");
                threadForFile.start();
            }
        });
        deleteFile.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    new FileFrame("delete");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        //////////////////////////////////////////////////////////Реализация кнопок выбора других калькуляторов
        engineerCalculator.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!checkOpenEngeneerCalc) {
                    EngineerCalcFrame.getInstanceEngineer();
                    animationForCalc("open");
                    checkOpenEngeneerCalc = true;
                }
            }
        });
    }

    private Timer timer1;
    private Timer timer2;

    synchronized void animationForHistory(String string) {
        if (string.equals("open")) {
            HistoryFrame.getInstanceHistory().setEndPosX(MainFrame.getInstanceMainWindow().getjFrame().getX() + 300);
            HistoryFrame.getInstanceHistory().setEndPosY(MainFrame.getInstanceMainWindow().getjFrame().getY());
        } else if (string.equals("close")) {
            HistoryFrame.getInstanceHistory().setEndPosX(MainFrame.getInstanceMainWindow().getjFrame().getX());
            HistoryFrame.getInstanceHistory().setEndPosY(MainFrame.getInstanceMainWindow().getjFrame().getY());
        }
        timer1 = new Timer(1, e -> {
            getjFrame().setAlwaysOnTop(true);
            if (HistoryFrame.getInstanceHistory().getStartPosX() <= HistoryFrame.getInstanceHistory().getEndPosX() + 2 &&
                    HistoryFrame.getInstanceHistory().getStartPosX() >= HistoryFrame.getInstanceHistory().getEndPosX() - 2 &&
                    //Для Y
                    HistoryFrame.getInstanceHistory().getStartPosY() <= HistoryFrame.getInstanceHistory().getEndPosY() + 2 &&
                    HistoryFrame.getInstanceHistory().getStartPosY() >= HistoryFrame.getInstanceHistory().getEndPosY() - 2) {
                timer1.stop();
                getjFrame().setAlwaysOnTop(false);
                if (string.equals("close")) {
                    HistoryFrame.getInstanceHistory().getjFrameHistory().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    HistoryFrame.getInstanceHistory().getjFrameHistory().dispose();
                    HistoryFrame.getInstanceHistory().deleteHistory();
                    MainFrame.getInstanceMainWindow().setCheckOpenHistory(false);
                }
            } else {
                HistoryFrame.getInstanceHistory().animationOpen(string);
            }
        });
        if (HistoryFrame.getInstanceHistory().getStartPosX() != HistoryFrame.getInstanceHistory().getEndPosX()) {
            timer1.start();
        }
    }

    synchronized void animationForCalc(String string) {
        if (string.equals("open")) {
            EngineerCalcFrame.getInstanceEngineer().setEndPosX(MainFrame.getInstanceMainWindow().getjFrame().getX() - 200);
            EngineerCalcFrame.getInstanceEngineer().setEndPosY(MainFrame.getInstanceMainWindow().getjFrame().getY());
        } else if (string.equals("close")) {
            EngineerCalcFrame.getInstanceEngineer().setEndPosX(MainFrame.getInstanceMainWindow().getjFrame().getX());
            EngineerCalcFrame.getInstanceEngineer().setEndPosY(MainFrame.getInstanceMainWindow().getjFrame().getY());
        }
        timer2 = new Timer(1, e -> {
            getjFrame().setAlwaysOnTop(true);
            if (EngineerCalcFrame.getInstanceEngineer().getStartPosX() <= EngineerCalcFrame.getInstanceEngineer().getEndPosX() + 2 &&
                    EngineerCalcFrame.getInstanceEngineer().getStartPosX() >= EngineerCalcFrame.getInstanceEngineer().getEndPosX() - 2 &&
                    //Для Y
                    EngineerCalcFrame.getInstanceEngineer().getStartPosY() <= EngineerCalcFrame.getInstanceEngineer().getEndPosY() + 2 &&
                    EngineerCalcFrame.getInstanceEngineer().getStartPosY() >= EngineerCalcFrame.getInstanceEngineer().getEndPosY() - 2) {
                timer2.stop();
                getjFrame().setAlwaysOnTop(false);
                if (string.equals("close")) {
                    EngineerCalcFrame.getInstanceEngineer().getjFrame().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    EngineerCalcFrame.getInstanceEngineer().getjFrame().dispose();
                    EngineerCalcFrame.getInstanceEngineer().deleteHistory();
                    checkOpenEngeneerCalc = false;
                }
            } else {
                EngineerCalcFrame.getInstanceEngineer().animationOpen(string);
            }
        });
        if (EngineerCalcFrame.getInstanceEngineer().getStartPosX() != EngineerCalcFrame.getInstanceEngineer().getEndPosX()) {
            timer2.start();
        }
    }

    @Override
    public void message(String string) {
        jTextRes.setText(jTextField.getText());
        jTextField.setText(string);
        arrayListToHistoty.add(jTextRes.getText());
        arrayListToHistoty.add("=");
        arrayListToHistoty.add(jTextField.getText());
        arrayListToHistoty.add(";");
    }

    void writeButtonNumbers(ActionEvent e) {
        //Запихнуть флаги
        if (unchangeableRes == 0) {
            jTextField.setText(jTextField.getText() + e.getActionCommand());
        }
    }

    private void writeButtonSigns(ActionEvent e) {
        if (onlyOneSign(jTextField.getText())) {
            unchangeableRes = 0;
            onlyOneDOt = 0;
            onlyOneZero = 0;
            writeButtonNumbers(e);

        }
    }

    private boolean onlyOneSign(String string) {
        Pattern pattern = Pattern.compile(".*[()0-9]$");
        Matcher matcher = pattern.matcher(string);
        return matcher.matches();
    }


    public static void main(String[] args) {
        MainFrame.getInstanceMainWindow();
    }
}
