package view;

import java.io.IOException;

public class ThreadForFile extends Thread {
    private String var = null;

    ThreadForFile(String s) {
        this.var = s;
    }

    @Override
    public void run() {
        if (var.equals("save")) {
            MainFrame.getInstanceMainWindow().getjFrame().setEnabled(false);
            try {
                new FileFrame("save");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (var.equals("open")) {
            try {
                new FileFrame("open");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
