package view;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

class jDialog extends JFrame {
    private JTextField textDialog = new JTextField();
    private static jDialog instance;

    static jDialog getInstanceDialog(String s) {
        if (instance == null) {
            instance = new jDialog(s);
        }
        return instance;
    }

    JTextField getTextDialog() {
        return textDialog;
    }

    private jDialog(String s) {
        JDialog jDialogWindow = new JDialog();
        jDialogWindow.setTitle("Warning");
        JPanel jPanelDialogAboutSave = new JPanel();
        jPanelDialogAboutSave.setLayout(null);
        jDialogWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        jDialogWindow.setSize(300, 210);
        jDialogWindow.setLocationRelativeTo(null);
        jDialogWindow.setResizable(false);
        jDialogWindow.add(jPanelDialogAboutSave);
        jDialogWindow.setVisible(true);
        textDialog.setHorizontalAlignment(SwingConstants.CENTER);
        textDialog.setBounds(20, 20, 250, 50);
        textDialog.setFont(new Font("Arial", Font.PLAIN, 14));
        textDialog.setEditable(false);
        textDialog.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 2));
        jPanelDialogAboutSave.add(textDialog);

        //////////////////////////////////////////////////
        if (s.equals("save")) {
            JButton ok = new JButton("OK");
            ok.setBounds(60, 100, 70, 50);
            JButton no = new JButton("NO");
            no.setBounds(170, 100, 70, 50);
            textDialog.setText("Вы точно хотите сохранить файл?");
            jPanelDialogAboutSave.add(ok);
            jPanelDialogAboutSave.add(no);
            ///////////////////////////////////////////Кнопка Warning
            ok.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    instance = null;
                    jDialogWindow.dispatchEvent(new WindowEvent(jDialogWindow, WindowEvent.WINDOW_CLOSING));
                    ThreadForFile thread = new ThreadForFile("save");
                    thread.start();
                }
            });
            no.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    MainFrame.getInstanceMainWindow().getjFrame().setEnabled(true);
                    jDialogWindow.dispatchEvent(new WindowEvent(jDialogWindow, WindowEvent.WINDOW_CLOSING));
                    instance = null;

                }
            });
        }
        if (s.equals("delete") || s.equals("saved")) {

            JButton yes = new JButton("ok");
            yes.setBounds(110, 100, 70, 50);
            jPanelDialogAboutSave.add(yes);
            yes.updateUI();

            yes.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    MainFrame.getInstanceMainWindow().getjFrame().setEnabled(true);
                    jDialogWindow.dispose();
                    instance = null;
                }
            });
            jDialogWindow.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    MainFrame.getInstanceMainWindow().getjFrame().setEnabled(true);
                    instance = null;
                }
            });
        }
    }
}
